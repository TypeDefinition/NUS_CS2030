public class Candle extends Entity {
    private enum State {
        FLICKER("Candle flickers."),
        SHORTER("Candle is getting shorter."),
        BURNING_OUT("Candle is about to burn out."),
        BURNED_OUT("Candle has burned out.");

        private final String description;

        private State(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }

        public State nextState() {
            int nextOrdinal = Math.min(values().length - 1, ordinal() + 1);
            return values()[nextOrdinal];
        }
    }
    
    private final State state;

    private Candle(State state) {
        super("Candle");
        this.state = state;
    }
        
    public Candle() {
        this(State.FLICKER);
    }

    @Override
    public String toString() {
        return state.getDescription();
    }

    @Override
    public Candle tick() {
        return new Candle(state.nextState());
    }
}
