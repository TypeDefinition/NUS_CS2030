public class Troll extends Entity {
    private enum State {
        LURK("Troll lurks in the shadows."),
        HUNGRY("Troll is getting hungry."),
        VERY_HUNGRY("Troll is VERY hungry."),
        SUPER_HUNGRY("Troll is SUPER HUNGRY and is about to ATTACK!"),
        ATTACK("Troll attacks!");

        private final String description;

        private State(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }

        public State nextState() {
            int nextOrdinal = Math.min(values().length - 1, ordinal() + 1);
            return values()[nextOrdinal];
        }
    }

    private final State state;

    private Troll(State state) {
        super("Troll");
        this.state = state;
    }

    public Troll() {
        this(State.LURK);
    }

    @Override
    public String toString() {
        return state.getDescription();
    }

    @Override
    public Troll tick() {
        return new Troll(state.nextState());
    }
}
