class Circle {
	private final Point center;
	private final double radius;

	public Circle(Point center, double radius) {
		this.center = center;
		this.radius = radius;	
	}

	public boolean inCircle(Point q) {
		return center.distanceToSquared(q) <= (radius * radius);
	}

	public int pointsInCircle(Point[] points) {
		int numPoints = 0;
		for (Point p : points) {
			numPoints += inCircle(p) ? 1 : 0;
		}
		return numPoints;
	}

	public String toString() {
		return "circle of radius " + radius + " centered at " + center.toString();
	}
}
