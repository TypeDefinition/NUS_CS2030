class Point {
	private final double x;
	private final double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "point (" + String.format("%.3f", this.x) + ", " + String.format("%.3f", this.y) + ")";
	}

	public Point midPoint(Point q) {
		double deltaX = q.x - this.x;
		double deltaY = q.y - this.y;
		return new Point(this.x + 0.5 * deltaX, this.y + 0.5 * deltaY);
	}

	public double angleTo(Point q) {
		double deltaX = q.x - this.x;
		double deltaY = q.y - this.y;
		return Math.atan2(deltaY, deltaX);
	}
	
	public double distanceToSquared(Point q) {
		double deltaX = q.x - this.x;
		double deltaY = q.y - this.y;
		return deltaX * deltaX + deltaY * deltaY;
	}

	public double distanceTo(Point q) {
		return Math.sqrt(distanceToSquared(q));
	}

	public Point moveTo(double theta, double d) {
		double deltaX = Math.cos(theta) * d;
		double deltaY = Math.sin(theta) * d;
		return new Point(this.x + deltaX, this.y + deltaY);
	}
}
