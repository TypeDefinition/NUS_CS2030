import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Iterable;
import java.util.Iterator;

class Schedule implements Iterable<NUSClass> {
    private final ArrayList<NUSClass> classList;

    public Schedule(List<NUSClass> classList) {
        this.classList = new ArrayList<NUSClass>(classList);
    }

    public Schedule() {
        this(new ArrayList<NUSClass>());
    }

    @Override
    public String toString() {
        String output = new String();
        for (NUSClass c : classList) {
           output += String.format("%s\n", c.toString());
        }
       return output;
    }

    public final List<NUSClass> getClassList() {
        return classList;
    }

    public Schedule add(NUSClass nusClass) {
       for (NUSClass c : classList) {
           if (c.clashWith(nusClass)) {
               return this;
           }
       }

       ArrayList<NUSClass> newList = new ArrayList<NUSClass>(classList);
       newList.add(nusClass);
       newList.sort(new NUSClassComparator());

       return new Schedule(newList);
    }

    @Override
    public Iterator<NUSClass> iterator() {
        return classList.iterator();
    }
}
