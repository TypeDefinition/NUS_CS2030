import java.util.Comparator;

class NUSClassComparator implements Comparator<NUSClass> {
    @Override
    public int compare(NUSClass a, NUSClass b) {
        if (a.clashWith(b)) {
            return 0;
        }

        if (a.getStartTime() < b.getStartTime()) {
            return -1;
        }

        if (b.getStartTime() < a.getStartTime()) {
            return 1;
        }

        if (a.getModuleCode().compareTo(b.getModuleCode()) < 0) {
            return -1;
        }

        if (b.getModuleCode().compareTo(a.getModuleCode()) < 0) {
            return 1;
        }

        if (a.getClassID() < b.getClassID()) {
            return -1;
        }

        if (b.getClassID() < a.getClassID()) {
            return 1;
        }

        return 0;
    }
}
