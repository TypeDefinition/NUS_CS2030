class Tutorial extends NUSClass {
    public Tutorial(String moduleCode, int classID, String venueID, Instructor instructor, int startTime) {
        super(moduleCode, classID, venueID, instructor, startTime);
    }

    @Override
    public String getIdentifier() {
        return String.format("T%d", getClassID());
    }

    @Override
    public int getDuration() {
        return 1;
    }

    @Override
    public boolean clashWith(NUSClass other) {
        if (!hasOverlappingTimeSlot(other)) {
            return false;
        }
        if (other instanceof Tutorial) {
            return hasSameVenue(other) || hasSameInstructor(other);
        }
        if (hasSameModule(other)) {
            return true;
        }
        if (hasSameVenue(other)) {
            return true;
        }
        if (hasSameInstructor(other)) {
            return true;
        }
        return false;
    }
}
