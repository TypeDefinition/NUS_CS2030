class Instructor {
    private final String name;
    
    public Instructor(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Instructor) {
            return this.name.equals(((Instructor)other).name);
        }
        return false;
    }

    public String getName() {
        return name;
    }
}
