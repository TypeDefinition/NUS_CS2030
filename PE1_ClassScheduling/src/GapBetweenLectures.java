import java.util.List;
import java.util.Iterator;

class GapBetweenLectures implements Constraint {
    public GapBetweenLectures() {
    }

    public boolean meetRequirements(NUSClass first, NUSClass second) {
        if (!(first instanceof Lecture)) {
            return true;
        }
        if (!(second instanceof Lecture)) {
            return true;
        }

        if (first.hasSameVenue(second)) {
            return second.getStartTime() - first.getEndTime() >= 1;
        }

        return true;
    }

    public boolean test(Schedule schedule) {
        Iterator<NUSClass> iter = schedule.iterator();

        // No classes? No clashes!
        if (!iter.hasNext()) {
            return true;
        }

        NUSClass first;
        NUSClass second = iter.next();
        while (iter.hasNext()) {
            first = second;
            second = iter.next();
            if (!meetRequirements(first, second)) {
                return false;
            }
        }
        
        return true;
    }
}
