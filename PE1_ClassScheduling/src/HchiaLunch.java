import java.util.List;
import java.util.Iterator;

class HchiaLunch implements Constraint {
    public HchiaLunch() {
    }

    public int getStartTime() {
        return 14;
    }

    public int getDuration() {
        return 2;
    }

    public int getEndTime() {
        return getStartTime() + getDuration();
    }

    public boolean hasOverlappingTimeSlot(NUSClass nusClass) {
        if (this.getStartTime() >= nusClass.getStartTime() && this.getStartTime() < nusClass.getEndTime()) {
            return true;
        }
        if (nusClass.getStartTime() >= this.getStartTime() && nusClass.getStartTime() < this.getEndTime()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean test(Schedule schedule) {
        for (NUSClass c : schedule) {
            if (hasOverlappingTimeSlot(c)) {
                return false;
            }
        }
        return true;
    }
}
