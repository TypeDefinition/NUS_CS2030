abstract class NUSClass {
    private final String moduleCode;
    private final int classID;
    private final String venueID;
    private final Instructor instructor;
    private final int startTime;
    
    public NUSClass(String moduleCode, int classID, String venueID, Instructor instructor, int startTime) {
        this.moduleCode = moduleCode;
        this.classID = classID;
        this.venueID = venueID;
        this.instructor = instructor;
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return String.format("%s %s @ %s [%s] %d--%d",
                getModuleCode(),
                getIdentifier(),
                getVenueID(),
                getInstructor().getName(),
                getStartTime(),
                getEndTime());
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public int getClassID() {
        return classID;
    }

    public String getVenueID() {
        return venueID;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return startTime + getDuration();
    }

    public boolean hasSameModule(NUSClass other) {
        return this.moduleCode.equals(other.moduleCode);
    }

    public boolean hasSameInstructor(NUSClass other) {
        return this.instructor.equals(other.instructor);
    }

    public boolean hasSameVenue(NUSClass other) {
        return this.venueID.equals(other.venueID);
    }

    public boolean hasOverlappingTimeSlot(NUSClass other) {
        if (this.getStartTime() >= other.getStartTime() && this.getStartTime() < other.getEndTime()) {
            return true;
        }
        if (other.getStartTime() >= this.getStartTime() && other.getStartTime() < this.getEndTime()) {
            return true;
        }
        return false;
    }

    abstract String getIdentifier();

    abstract int getDuration();

    abstract boolean clashWith(NUSClass other);
}
