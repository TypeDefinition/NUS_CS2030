import java.util.function.Function;
import java.util.function.Predicate;

interface Logger<T> {
    public static <U> Logger<U> make(U value) {
        if (value == null) {
            throw new IllegalArgumentException("argument cannot be null");
        }
        if (value instanceof Logger) {
            throw new IllegalArgumentException("already a Logger");
        }

        // Create our logs.
        String[] logs = new String[1];
        logs[0] = String.format("Value initialized. Value = %s", value.toString());
        return new LoggerImpl<U>(value, logs);
    }

    public T getValue();

    public String[] getLogs();

    public void printlog();

    public <U> Logger<U> map(Function<? super T, ? extends U> mapper);

    public <U> Logger<U> flatMap(Function<? super T, ? extends Logger<? extends U>> mapper);

    public boolean test(Predicate<? super T> p);
}
