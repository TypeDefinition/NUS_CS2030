Logger<Integer> add(Logger<Integer> a, int b) {
    return a.map((value) -> { return value + b; });
}

Logger<Integer> sum(int n) {
    if (n < 0) {
        throw new IllegalArgumentException("n must be a positive integer");
    }

    // Version 1. The first answer I thought of.
    // return (n == 0) ? Logger.make(0) : add(sum(n - 1), n);

    /* Version 2. Improving upon the previous version.
    If n is a negative number, it returns Logger.make(n)
    rather then continue looping endlessly. But invalid
    values of n should never reach this line. */
    return (n > 0) ? add(sum(n - 1), n) : Logger.make(n);
}

Logger<Integer> f(int n) {
    if (n < 0) {
        throw new IllegalArgumentException("n must be a positive integer");
    }

    // Predicate(s)
    Predicate<Integer> isOne = (value) -> { return value == 1; };
    Predicate<Integer> isEven = (value) -> { return value % 2 == 0; };

    // Function(s)
    Function<Integer, Integer> add1 = (value) -> { return value + 1; };
    Function<Integer, Integer> divide2 = (value) -> { return value / 2; };
    Function<Integer, Integer> multiply3 = (value) -> { return value * 3; };

    // Need to wrap f in a Function because calling logger.flatMap(f) directly does not compile.
    Function<Integer, Logger<Integer>> collatzConjecture = (value) -> { return f(value); };

    Logger<Integer> logger = Logger.make(n);
    if (logger.test(isOne)) {
        return logger;
    } else if (logger.test(isEven)) {
        return logger.map(divide2).flatMap(collatzConjecture); 
    }
    return logger.map(multiply3).map(add1).flatMap(collatzConjecture);
}
