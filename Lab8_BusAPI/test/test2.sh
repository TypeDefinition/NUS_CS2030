#!/bin/sh

#This MUST be run from the src folder. This script assumes that the file structure is not changed.

mkdir sandbox
cp *.class sandbox/
cp ../test/level2.jar sandbox/
cd sandbox

jar uf level2.jar *.class
echo "16189 Clementi" | java -jar level2.jar

cd ../
rm -r sandbox
