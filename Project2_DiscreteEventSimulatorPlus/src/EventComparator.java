package cs2030.simulator;

import java.util.Comparator;

/**
 * Compares 2 Events for sorting their priority.
 */
public class EventComparator implements Comparator<Event> {
    /**
     * Compares 2 Events for order. Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal to,
     * or greater than the second. The events are first sorted by the time they
     * occur. If 2 event occurs at the same time, they are sorted by the time
     * their customer arrived.
     * @param eventA first event to be compared.
     * @param eventB second event to be compared.
     */ 
    @Override
    public int compare(Event eventA, Event eventB) {
        if (eventA.eventTime() < eventB.eventTime()) {
            return -1;
        } else if (eventA.eventTime() > eventB.eventTime()) {
            return 1;
        }

        if (eventA.customerArrivalTime() < eventB.customerArrivalTime()) {
            return -1;
        } else if (eventA.customerArrivalTime() > eventB.customerArrivalTime()) {
            return 1;
        }

        return 0;
    }
}
