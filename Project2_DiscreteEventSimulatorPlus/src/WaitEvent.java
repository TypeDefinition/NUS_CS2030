package cs2030.simulator;

/**
 * Represents a customer waiting.
 */
public class WaitEvent extends Event {
    /**
     * The customer that the event represents.
     */
    private final Customer customer;
    /**
     * The server the customer is waiting for.
     */
    private final Server server;
    
    /**
     * Create a new WaitEvent object.
     * @param customer The customer that the event represents.
     * @param server The server the customer is waiting for.
     */
    public WaitEvent(Customer customer, Server server) {
        super(customer.arrivalTime(), customer.arrivalTime(),
            (shop) -> {
                // Debug Output
                /* System.out.println(
                    String.format("Before %d Wait for %s",
                    customer.id(), server.name()));
                shop.print(); */
        
                Shop newShop = shop.wait(server.identifier());
                Server newServer = newShop.nextAvailableServer(server.identifier());
                
                // Debug Output            
                /* System.out.println(
                    String.format("After %d Wait for %s until %.3f",
                    customer.id(), server.name(), nextAvailableTime));
                newShop.print(); */
            
                return Pair.of(newShop,
                    new ServeEvent(newServer.nextAvailableTime(), customer, newServer));
            });

        this.customer = customer;
        this.server = server;
    }
    
    @Override
    public String toString() {
        return String.format("%.3f %s waits to be served by %s",
                eventTime(), customer.name(), server.name());
    }
}
