package cs2030.simulator;

/**
 * Represent the customer leaving.
 */
public class LeaveEvent extends Event {
    /**
     * The customer that the event represents.
     */
    private final Customer customer;

    /**
     * Create a new LeaveEvent object.
     * @param customer The customer that the event represents.
     */
    public LeaveEvent(Customer customer) {
        super(customer.arrivalTime(), customer.arrivalTime(),
            (shop) -> {
                // Debug Output
                /* System.out.println(String.format("Before %d Leave", customer.id()));
                shop.print(); */
            
                // Debug Output
                /* System.out.println(String.format("After %d Leave", customer.id()));
                shop.print(); */
        
                return Pair.of(shop, new EndEvent());
            });

        this.customer = customer;
    }
    
    @Override
    public String toString() {
        return String.format("%.3f %s leaves", eventTime(), customer.name());
    }
}
