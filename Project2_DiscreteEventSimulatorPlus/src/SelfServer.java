package cs2030.simulator;

import java.util.function.Supplier;

/**
 * A self-check server that does not need rest.
 */
public class SelfServer extends Server {
    /**
     * Supplies the service duration of the server.
     */
    private final Supplier<Double> serviceDuration;
    
    /**
     * Create a new SelfServer object.
     * @param identifier The server id.
     * @param isAvailable Server available flag.
     * @param nextAvailableTime Server next available time.
     * @param queue Server customer queue.
     * @param serviceDuration Supplies the service duration of the server.
     */
    public SelfServer(int identifier, boolean isAvailable,
        double nextAvailableTime, CustomerQueue queue,
        Supplier<Double> serviceDuration) {
        super(identifier, isAvailable, nextAvailableTime, queue);
        this.serviceDuration = serviceDuration;
    }
    
    @Override
    public SelfServer setQueue(CustomerQueue queue) {
        return new SelfServer(identifier(), isAvailable(),
            nextAvailableTime(), queue, serviceDuration);
    }
    
    @Override
    public SelfServer queue() {
        return new SelfServer(identifier(), isAvailable(),
            nextAvailableTime(), getQueue().add(), serviceDuration);
    }
    
    @Override
    public SelfServer dequeue() {
        return new SelfServer(identifier(), isAvailable(),
            nextAvailableTime(), getQueue().remove(), serviceDuration);
    }
    
    @Override
    public SelfServer serve(double serveStartTime) {
        return new SelfServer(identifier(), false,
            serveStartTime + serviceDuration.get(),
            getQueue().remove(), serviceDuration);
    }
    
    @Override
    public SelfServer done() {
        return new SelfServer(identifier(), true,
            nextAvailableTime(), getQueue(), serviceDuration);
    }
 
    @Override
    public final String name() {
        return String.format("self-check %d", identifier());
    } 
    
    @Override
    public boolean useSharedQueue() {
        return true;
    }
}
