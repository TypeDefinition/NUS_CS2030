package cs2030.simulator;

import java.util.function.Function;

/**
 * Base class of events.
 */
public abstract class Event {
    /**
     * The time the event occured.
     */
    private final double eventTime;
    /**
     * The time the customer arrived.
     */
    private final double customerArrivalTime;
    /**
     * The lamda to execute.
     */
    private final Function<Shop, Pair<Shop, Event>> execute;

    /**
     * Abstract constructor.
     * @param execute The lamda to execute.
     */
    public Event(Function<Shop, Pair<Shop, Event>> execute) {
        this(0.0, 0.0, execute);
    }

    /**
     * Abstract constructor.
     * @param eventTime The time the event occured.
     * @param customerArrivalTime The time the customer arrived.
     * @param execute The lamda to execute.
     */
    public Event(double eventTime, double customerArrivalTime,
        Function<Shop, Pair<Shop, Event>> execute) {
        this.eventTime = eventTime;
        this.customerArrivalTime = customerArrivalTime;
        this.execute = execute;
    }

    /**
     * Execute the event.
     * @param shop The shop containing the servers.
     */
    public final Pair<Shop, Event> execute(Shop shop) {
        /* There are 3 phase of every event.
         * 1. Creation of event instance. The event has NOT occured.
         * 2. Execute. The event has occured.
         * 3. Creating the next event instance. */
        return execute.apply(shop);
    }

    public double customerArrivalTime() {
        return customerArrivalTime;
    }
    
    public double eventTime() {
        return eventTime;
    }
    
    /**
     * Get the duration from the customer arriving to this event executing.
     * @return The duration from the customer arriving to this event occuring.
     */
    public double timeToEvent() {
        return eventTime() - customerArrivalTime();
    }
    
    /**
     * Returns a string representation of the object.
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        return "Base Event";
    }
}
