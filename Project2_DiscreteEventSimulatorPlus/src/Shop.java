package cs2030.simulator;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.function.Predicate;
import java.util.Comparator;

/**
 * The shop that contains the list of servers.
 */
public class Shop {
    /**
     * List of servers.
     */
    private final List<Server> serverList;

    /**
     * A comparator used to sort the servers by next available time.
     */
    private final Comparator<Server> sortNextAvailableTime =
        (a, b) -> {
            if (a.nextAvailableTime() < b.nextAvailableTime()) {
                return -1;
            }
            if (a.nextAvailableTime() > b.nextAvailableTime()) {
                return 1;
            }
            
            if (a.identifier() < b.identifier()) {
                return -1;
            }
            if (a.identifier() > b.identifier()) {
                return 1;
            }
            
            return 0;
        };

    /**
     * Create a new Shop object.
     * @param serverList The list of servers to initialise the shop with.
     */
    public Shop(List<Server> serverList) {
        this.serverList = Optional
            .ofNullable(serverList)
            .orElse(new ArrayList<Server>());
    }

    /**
     * Create a new Shop object.
     * @param numServers The number of default servers to initialise the shop with.
     */
    public Shop(int numServers) {
        this.serverList = IntStream.rangeClosed(1, numServers)
            .boxed()
            .map((i) -> new Server(i, true, false, 0.0))
            .collect(Collectors.toList());
    }

    /**
     * Returns a string representation of the object.
     * @return A string representation of the object.
     */
    @Override
    public String toString() {
        return serverList.toString();
    }

    /**
     * Find the first server in the list that satisfy the predicate.
     * @param predicate The predicate to check the servers against.
     * @return Returns an Optional containing the
     *      first server in the list that satisfy the predicate.
     *      If none are found, return an empty Optional.
     */
    public Optional<Server> find(Predicate<Server> predicate) {
        return serverList.stream().filter(predicate).findFirst();
    }
    
    /**
     * Get the list of servers in the shop.
     * @return The list of servers in the shop.
     */
    public List<Server> getServerList() {
        return serverList;
    }

    /**
     * Replace the server in the server list which has the
     * same identifier as the server passed in the parameters.
     * @param server The new server to replace in the server list of the shop.
     * @return A shop with the updated server.
     */
    public Shop replace(Server server) {
        List<Server> newServerList = serverList.stream()
            .map((s) -> {
                return s.identifier() == server.identifier() ? server : s;
            })
            .collect(Collectors.toList());
        return new Shop(newServerList);
    }
    
    /**
     * Get the server in the shop with the given identifier.
     * @param serverIdentifier The identifier of the server to get.
     * @return The server in the shop with the given identifier.
     */
    public Server get(int serverIdentifier) {
        Predicate<Server> compareIdentifier = (s) -> {
            return s.identifier() == serverIdentifier;
        };
        return find(compareIdentifier).get();
    }
    
    /**
     * Adds one to the queue of a server.
     * If the server identifier belongs to a self check server,
     * all the self-check servers have one added to their queue.
     * @param serverIdentifier The identifier of the server to wait.
     * @return A shop with the updated server.
     */
    public Shop wait(int serverIdentifier) {
        Server server = get(serverIdentifier);
        if (!server.useSharedQueue()) {
            return replace(server.queue());
        }

        // Get New Server
        Server newServer = server.queue();
        // Get New Queue        
        CustomerQueue newQueue = newServer.getQueue();
        
        List<Server> newServerList = serverList.stream()
            // Update Server List With New Server
            .map((s) -> {
                return s.identifier() == newServer.identifier() ? newServer : s;
            })
            // Update Server List With New Queue
            .map((s) -> {
                return s.useSharedQueue() ? s.setQueue(newQueue) : s;
            })
            .collect(Collectors.toList());
            
        return new Shop(newServerList);
    }
    
    /**
     * Sets the state of the server to serve.
     * @param serverIdentifier The identifier of the server to set to serve.
     * @return A pair containing the shop with the updated server and the server.
     */
    public Pair<Shop, Server> serve(int serverIdentifier, double serveStartTime) {
        Server server = get(serverIdentifier);
        if (!server.useSharedQueue()) {
            Server newServer = server.serve(serveStartTime);
            Shop newShop = replace(newServer);
            return Pair.of(newShop, newServer);
        }
        
        // Get New Server
        Server newServer = serverList.stream()
            .filter(Server::useSharedQueue)
            .filter(Server::isAvailable)
            .findFirst()
            .map((s) -> s.serve(serveStartTime)).get();
            
        // Get New Queue
        CustomerQueue newQueue = newServer.getQueue();
        
        List<Server> newServerList = serverList.stream()
            // Update Server List With New Server
            .map((s) -> {
                return s.identifier() == newServer.identifier() ? newServer : s;
            })
            // Update Server List With New Queue
            .map((s) -> {
                return s.useSharedQueue() ? s.setQueue(newQueue) : s;
            })
            .collect(Collectors.toList());

        // Get New Shop
        Shop newShop = new Shop(newServerList);
        
        // Get New New Server
        Server newNewServer = newShop.get(newServer.identifier());

        return Pair.of(newShop, newNewServer);
    }
    
    /**
     * Get the availability of a server. If the server if a self-check,
     * returns true if any of the self-check is available.
     * @param serverIdentifier The identifier of the server to query.
     * @return The availability of the server.
     */
    public boolean isAvailable(int serverIdentifier) {
        Server server = get(serverIdentifier);
        
        if (!server.useSharedQueue()) {
            return server.isAvailable();
        }
        
        return serverList.stream()
                .filter(Server::useSharedQueue)
                .anyMatch(Server::isAvailable);
    }
    
    /**
     * If the specified server identifier is a self-check, return the
     * next earliest available self-check server.
     * If the specified server identifier is a human server,
     * return it directly.
     * @param serverIdentifier The identifier of the server to query.
     * @return If the specified server identifier is a self-check, return the
     *      next earliest available self-check server.
     *      If the specified server identifier is a human server,
     *      return it directly.
     */
    public Server nextAvailableServer(int serverIdentifier) {
        Server server = get(serverIdentifier);
        
        if (!server.useSharedQueue()) {
            return server;
        }
        
        return serverList.stream()
                .filter(Server::useSharedQueue)
                .sorted(sortNextAvailableTime)
                .findFirst().get();
    }
    
    /**
     * If the specified server identifier is a self-check, return the
     * next earliest available self-check server's next available time.
     * If the specified server identifier is a human server,
     * return it's next available time.
     * @param serverIdentifier The identifier of the server to query.
     * @return If the specified server identifier is a self-check, return the
     *      next earliest available self-check server's next available time.
     *      If the specified server identifier is a human server,
     *      return it's next available time.
     */
    public double nextAvailableTime(int serverIdentifier) {
        Server server = get(serverIdentifier);
        
        if (!server.useSharedQueue()) {
            return server.nextAvailableTime();
        }
        
        return serverList.stream()
                .filter(Server::useSharedQueue)
                .sorted(sortNextAvailableTime)
                .findFirst().get().nextAvailableTime();
    }
    
    /**
     * Set the state of the specify server to done.
     * @param serverIdentifier The identifier of the server to set.
     * @return The shop with the server updated.
     */
    public Shop done(int serverIdentifier) {
        Server newServer = get(serverIdentifier).done();
        return replace(newServer);
    }
    
    /**
     * Set the state of the specify server to rest.
     * @param serverIdentifier The identifier of the server to set.
     * @return The shop with the server updated.
     */
    public Shop rest(int serverIdentifier, double restStartTime) {
        Server newServer = get(serverIdentifier).rest(restStartTime);
        return replace(newServer);
    }
    
    /**
     * Set the state of the specify server to back from rest.
     * @param serverIdentifier The identifier of the server to set.
     * @return The shop with the server updated.
     */
    public Shop back(int serverIdentifier) {
        Server newServer = get(serverIdentifier).back();
        return replace(newServer);
    }
    
    /**
     * Print debug information of the servers.
     */
    public void printDebug() {
        serverList.stream().forEachOrdered((s) -> {
            System.out.println(
                String.format("[%s] Queue: %d, Available: %b",
                s.name(), s.queueSize(), s.isAvailable()));
        });
    }
}
