package cs2030.simulator;

/**
 * Container for a pair of objects.
 */
public class Pair<T, U> {
    /**
     * First objects.
     */
    private final T first;
    /**
     * Second objects.
     */
    private final U second;

    /**
     * Create a new Pair object.
     * @param first The first objects.
     * @param second The second objects.
     */
    private Pair(T first, U second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Create a new Pair object.
     * @param first The first objects.
     * @param second The second objects.
     * @return A new Pair object containing the 2 given objects.
     */
    public static <V, W> Pair<V, W> of(V first, W second) {
        return new Pair<V, W>(first, second);
    }

    public T first() {
        return this.first;
    }

    public U second() {
        return this.second;
    }
}
