package cs2030.simulator;

import java.util.function.Supplier;

/**
 * Represents the rescheduling of events.
 */
public class RescheduleEvent extends Event {
    /**
     * Create a new RescheduleEvent object.
     * @param eventSupplier The event to reschedule.
     */
    public RescheduleEvent(Supplier<Event> eventSupplier) {
        super((shop) -> {
            return Pair.of(shop, eventSupplier.get());
        });
    }
    
    @Override
    public String toString() {
        return "Reschedule Event";
    }
}
