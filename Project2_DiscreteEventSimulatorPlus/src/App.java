package cs2030.simulator;

import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.HashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.Consumer;

/**
 * The simulation application.
 */
public class App {
    /**
     * Random number generator.
     */
    private final RandomGenerator rng;
    
    /**
     * An int value denoting the base seed for the RandomGenerator object.
     */
    private int seed;
    /**
     * An int value representing the number of servers.
     */
    private int numHumanServers;
    /**
     * An int value representing the number of self-checkout counters, Nself.
     */
    private int numSelfCheck;
    /**
     * An int value for the maximum queue length, Qmax.
     */
    private int maxQueueLength;
    /**
     * An int representing the number of customers (or the number of arrival events) to simulate.
     */
    private int numCustomers;
    /**
     * A positive double parameter for the arrival rate, λ.
     */
    private double arrivalRate;
    /**
     * A positive double parameter for the service rate, μ.
     */
    private double serviceRate;
    /**
     * A positive double parameter for the resting rate, ρ.
     */
    private double restingRate;
    /**
     * A double parameter for the probability of resting, Pr.
     */
    private double restingProbability;
    /**
     * A double parameter for the probability of a greedy customer occurring, Pg.
     */
    private double greedyCustomerProbability;
    /**
     * A hashmap containing the handling of the different possible accepted inputs.
     */
    private final HashMap<Integer, Consumer<String[]>> inputMap;
    
    /**
     * Create a new App object.
     * @param args The command line inputs. 
     */
    public App(String[] args) {
        this.inputMap = new HashMap<Integer, Consumer<String[]>>();
        initInputMap();
        inputMap.get(args.length).accept(args);
        rng = new RandomGenerator(seed, arrivalRate, serviceRate, restingRate);
    }
    
    /**
     * Initialise the input handling hashmap.
     */
    private void initInputMap() {
        // Level 3
        inputMap.put(5,
            (String[] args) -> {
                seed = Integer.parseInt(args[0]);
                numHumanServers = Integer.parseInt(args[1]);
                numSelfCheck = 0;
                maxQueueLength = 1;
                numCustomers = Integer.parseInt(args[2]);
                arrivalRate = Double.parseDouble(args[3]);
                serviceRate = Double.parseDouble(args[4]);
                restingRate = 0.0;
                restingProbability = 0.0;
                greedyCustomerProbability = 0.0;
            });
                
        // Level 4
        inputMap.put(6,
            (String[] args) -> {
                seed = Integer.parseInt(args[0]);
                numHumanServers = Integer.parseInt(args[1]);
                numSelfCheck = 0;
                maxQueueLength = Integer.parseInt(args[2]);
                numCustomers = Integer.parseInt(args[3]);
                arrivalRate = Double.parseDouble(args[4]);
                serviceRate = Double.parseDouble(args[5]);
                restingRate = 0.0;
                restingProbability = 0.0;
                greedyCustomerProbability = 0.0;
            });
                
        // Level 5
        inputMap.put(8,
            (String[] args) -> {
                seed = Integer.parseInt(args[0]);
                numHumanServers = Integer.parseInt(args[1]);
                numSelfCheck = 0;
                maxQueueLength = Integer.parseInt(args[2]);
                numCustomers = Integer.parseInt(args[3]);
                arrivalRate = Double.parseDouble(args[4]);
                serviceRate = Double.parseDouble(args[5]);
                restingRate = Double.parseDouble(args[6]);
                restingProbability = Double.parseDouble(args[7]);
                greedyCustomerProbability = 0.0;
            });
                
        // Level 6
        inputMap.put(9,
            (String[] args) -> {
                seed = Integer.parseInt(args[0]);
                numHumanServers = Integer.parseInt(args[1]);
                numSelfCheck = Integer.parseInt(args[2]);
                maxQueueLength = Integer.parseInt(args[3]);
                numCustomers = Integer.parseInt(args[4]);
                arrivalRate = Double.parseDouble(args[5]);
                serviceRate = Double.parseDouble(args[6]);
                restingRate = Double.parseDouble(args[7]);
                restingProbability = Double.parseDouble(args[8]);
                greedyCustomerProbability = 0.0;
            });
                
        // Level 7
        inputMap.put(10,
            (String[] args) -> {
                seed = Integer.parseInt(args[0]);
                numHumanServers = Integer.parseInt(args[1]);
                numSelfCheck = Integer.parseInt(args[2]);
                maxQueueLength = Integer.parseInt(args[3]);
                numCustomers = Integer.parseInt(args[4]);
                arrivalRate = Double.parseDouble(args[5]);
                serviceRate = Double.parseDouble(args[6]);
                restingRate = Double.parseDouble(args[7]);
                restingProbability = Double.parseDouble(args[8]);
                greedyCustomerProbability = Double.parseDouble(args[9]);
            });
    }
    
    /**
     * Run the application.
     */
    public void run() {
        // Create priority queue.
        PriorityQueue<Event> priorityQueue = new PriorityQueue<Event>(new EventComparator());
        double arrivalTime = 0.0;
        for (int i = 1; i <= numCustomers; i++) {
            Customer customer = (rng.genCustomerType() < greedyCustomerProbability) ?
                    new GreedyCustomer(i, arrivalTime) : new Customer(i, arrivalTime);
            arrivalTime += rng.genInterArrivalTime();

            priorityQueue.add(new ArriveEvent(customer));
        }
        
        List<Server> serverList = new ArrayList<Server>();
        for (int i = 1; i <= numHumanServers; i++) {
            Supplier<Double> serviceDuration = () -> {
                return rng.genServiceTime();
            };
            Supplier<Boolean> needRest = () -> {
                return rng.genRandomRest() < restingProbability;
            };
            Supplier<Double> restDuration = () -> {
                return rng.genRestPeriod();
            };
            
            CustomerQueue queue = new CustomerQueue(maxQueueLength, 0);
            Server humanServer = new HumanServer(i, true, 0.0,
                queue, serviceDuration, needRest, restDuration);            
            serverList.add(humanServer);
        }
        
        for (int i = 1; i <= numSelfCheck; i++) {
            Supplier<Double> serviceDuration = () -> {
                return rng.genServiceTime();
            };

            CustomerQueue queue = new CustomerQueue(maxQueueLength, 0);
            SelfServer selfServer = new SelfServer(i + numHumanServers,
                true, 0.0, queue, serviceDuration);
            serverList.add(selfServer);
        }

        // Run simulation.
        double totalWaitTime = 0.0;
        int serveCounter = 0;
        int leaveCounter = 0;

        Shop shop = new Shop(serverList);
        while (!priorityQueue.isEmpty()) {
            Event event = priorityQueue.poll();
            
            if (event instanceof EndEvent) {
                continue;
            }
            
            Pair<Shop, Event> pair = event.execute(shop);
            shop = pair.first();
            Event nextEvent = pair.second();

            if (!(event instanceof RescheduleEvent) &&
                !(nextEvent instanceof RescheduleEvent)) {
                if (event instanceof ServeEvent) {
                    totalWaitTime += event.timeToEvent();
                    ++serveCounter;
                }
                
                if (event instanceof LeaveEvent) {
                    ++leaveCounter;
                }
            }

            if (!(event instanceof ServerRestEvent) &&
                !(event instanceof ServerBackEvent) &&
                !(event instanceof RescheduleEvent) &&
                !(nextEvent instanceof RescheduleEvent)) {
                System.out.println(event.toString());
            }
            
            priorityQueue.add(nextEvent);
        }
        
        double averageWaitTime = totalWaitTime / serveCounter;
        System.out.println(String.format("[%.3f %d %d]",
                Double.isNaN(averageWaitTime) ? 0.0 : averageWaitTime,
                serveCounter,
                leaveCounter));
    }
}
