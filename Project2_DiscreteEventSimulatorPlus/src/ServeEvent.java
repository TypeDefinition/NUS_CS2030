package cs2030.simulator;

import java.util.function.Supplier;

/**
 * Represents a server serving a customer.
 */
public class ServeEvent extends Event {
    /**
     * The customer that the event represents.
     */
    private final Customer customer;
    /**
     * The server serving the customer.
     */
    private final Server server;

    /**
     * Create a new ServeEvent object.
     * @param eventTime The time the event occured.
     * @param customer The customer that the event represents.
     * @param server The server serving the customer.
     */
    public ServeEvent(double eventTime, Customer customer, Server server) {
        super(eventTime, customer.arrivalTime(),
            (shop) -> {
                if (shop.isAvailable(server.identifier())) {
                    // Debug Output
                    /* System.out.println(String.format("Before %d Serve by %s",
                        customer.id(), server.name()));
                    shop.print();*/
                
                    Pair<Shop, Server> pair = shop.serve(server.identifier(), eventTime);
                    Shop newShop = pair.first();
                    Server newServer = pair.second();

                    // Debug Output
                    /* System.out.println(String.format("After %d Serve by %s",
                        customer.id(), newServer.name()));
                    newShop.print(); */
                
                    return Pair.of(newShop,
                        new DoneEvent(newServer.nextAvailableTime(), customer, newServer));
                }

                // Reschedule
                Supplier<Event> eventSupplier = () -> {
                    Server newServer = shop.nextAvailableServer(server.identifier());
                    return new ServeEvent(newServer.nextAvailableTime(), customer, newServer);
                };
                
                return Pair.of(shop, new RescheduleEvent(eventSupplier));
            });
        
        this.customer = customer;
        this.server = server;
    }
    
    @Override
    public String toString() {
        return String.format("%.3f %s served by %s",
            eventTime(), customer.name(), server.name());
    }
}
