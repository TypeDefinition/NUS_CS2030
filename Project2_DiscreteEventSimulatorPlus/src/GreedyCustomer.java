package cs2030.simulator;

import java.util.Optional;
import java.util.Comparator;

/**
 * A Greedy Customer.
 */
public class GreedyCustomer extends Customer {
    /**
     * Create a new GreedyCustomer object.
     * @param id This is the customer's ID.
     *     It is assigned in chronological order of arrival.
     * @param arrivalTime The time the customer arrives.
     */
    public GreedyCustomer(int id, double arrivalTime) {
        super(id, arrivalTime);
    }

    @Override
    public String toString() {
        return String.format("%d(greedy)", id());
    }

    @Override
    public String name() {
        return String.format("%d(greedy)", id());
    }
    
    @Override
    public Optional<Server> chooseQueue(Shop shop) {
        Comparator<Server> sortQueueSize =
            (a, b) -> {
                if (a.queueSize() < b.queueSize()) {
                    return -1;
                }
                if (a.queueSize() > b.queueSize()) {
                    return 1;
                }
            
                if (a.identifier() < b.identifier()) {
                    return -1;
                }
                if (a.identifier() > b.identifier()) {
                    return 1;
                }
                return 0;
            };
    
        return shop.getServerList().stream()
            .filter(Server::queueAvailable)
            .sorted(sortQueueSize).findFirst();
    }
}
