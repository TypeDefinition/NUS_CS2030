package cs2030.simulator;

/**
 * Represents a server done serving a customer.
 */
public class DoneEvent extends Event {
    /**
     * The customer that the event represents.
     */
    private final Customer customer;
    /**
     * The server that served the customer.
     */
    private final Server server;
    
    /**
     * Create a new DoneEvent object.
     * @param eventTime The time the event occured.
     * @param customer The customer that the event represents.
     * @param server The server that served the customer.
     */
    public DoneEvent(double eventTime, Customer customer, Server server) {
        super(eventTime, customer.arrivalTime(),
            (shop) -> {
                // Debug Output
                /* System.out.println(String.format("Before %d Done", customer.id()));
                shop.print(); */
        
                Shop newShop = shop.done(server.identifier());
                Server newServer = shop.get(server.identifier());
                
                // Debug Output
                /* System.out.println(String.format("After %d Done", customer.id()));
                newShop.print(); */
            
                if (newServer.needRest()) {
                    return Pair.of(newShop, new ServerRestEvent(eventTime, newServer));
                }
            
                return Pair.of(newShop, new EndEvent());
            });

        this.customer = customer;
        this.server = server;
    }
    
    @Override
    public String toString() {
        return String.format("%.3f %s done serving by %s",
                eventTime(), customer.name(), server.name());
    }
}
