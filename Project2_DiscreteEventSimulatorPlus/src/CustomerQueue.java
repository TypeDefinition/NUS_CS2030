package cs2030.simulator;

/**
 * A customer queue.
 */
public class CustomerQueue {
    /**
     * Max size of the queue.
     */
    private final int maxSize;
    /**
     * Current size of the queue.
     */
    private final int size;

    /**
     * Create a new CustomerQueue object.
     * @param maxSize Max size of the queue.
     * @param size Current size of the queue
     */
    public CustomerQueue(int maxSize, int size) {
        this.maxSize = Math.max(maxSize, 0);
        this.size = Math.min(size, maxSize);
    }

    public int maxSize() {
        return maxSize;
    }
    
    public int size() {
        return size;
    }

    /**
     * Add one to the queue.
     * @return The modified queue.
     */
    public CustomerQueue add() {
        return new CustomerQueue(maxSize, Math.min(size + 1, maxSize));
    }
    
    /**
     * Remove one from the queue.
     * @return The modified queue.
     */
    public CustomerQueue remove() {
        return new CustomerQueue(maxSize, Math.max(size - 1, 0));
    }
    
    public boolean empty() {
        return size == 0;
    }
    
    public boolean full() {
        return size == maxSize;
    }
}
