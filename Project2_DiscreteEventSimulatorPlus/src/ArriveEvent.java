package cs2030.simulator;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Represents a customer arriving.
 */
public class ArriveEvent extends Event {
    /**
     * The customer that the event represents.
     */
    private final Customer customer;
  
    /**
     * Create a new ArriveEvent object.
     * @param customer The customer that the event represents.
     */
    public ArriveEvent(Customer customer) {
        // Phase 1: Creation of event instance.
        super(customer.arrivalTime(), customer.arrivalTime(),
            (shop) -> {
                // Debug Output
                /* System.out.println(String.format("Before %d Arrive", customer.id()));
                shop.print(); */
        
                // Serve Event
                Function<Server, Pair<Shop, Event>> serve = (s) -> {
                    return Pair.of(shop, new ServeEvent(customer.arrivalTime(), customer, s));
                };
            
                // Wait Event
                Function<Server, Pair<Shop, Event>> wait = (s) -> {
                    return Pair.of(shop, new WaitEvent(customer, s));
                };
            
                // Leave Event
                Supplier<Pair<Shop, Event>> leave = () -> {
                    return Pair.of(shop, new LeaveEvent(customer));
                };
            
                // Debug Output
                /* System.out.println(String.format("After %d Arrive", customer.id()));
                shop.print(); */
            
                return
                    // Phase 2: Event has occured. But in the case of ArriveEvent, Nothing happens.
                    // Do nothing.
                    // Phase 3: Creating the next event instance.
                    // If there is a server available, serve.
                    shop.find(Server::isAvailable).map(serve)
                    // Otherwise, if there is a server the customer can queue at, wait.
                    .or(() -> customer.chooseQueue(shop).map(wait))
                    // Else, leave.
                    .orElseGet(leave);
            });
        
        this.customer = customer;
    }
    
    @Override
    public String toString() {
        return String.format("%.3f %s arrives", eventTime(), customer.name());
    }
}
