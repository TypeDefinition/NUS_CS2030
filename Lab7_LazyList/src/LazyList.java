import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

class LazyList<T extends Comparable<T>> {
    private final int maxSize;
    private final ArrayList<Lazy<T>> list;
    private final UnaryOperator<T> unaryOp;

    private LazyList(int maxSize, T seed, UnaryOperator<T> unaryOp) {
        // Initialise varaible(s)
        this.maxSize = maxSize;
        this.list = new ArrayList<Lazy<T>>();
        this.unaryOp = unaryOp;

        // Create the first element of the list.
        Lazy<T> head = Lazy.of(seed);
        list.add(head);
    }

    static <U extends Comparable<U>> LazyList<U> generate(int maxSize, U seed, UnaryOperator<U> unaryOp) {
        return new LazyList<U>(maxSize, seed, unaryOp);
    }

    public T get(int index) {
        if (index >= maxSize) {
            throw new IndexOutOfBoundsException();
        }

        /* To get the element at index n,
        we need to ensure our list is at least of size n + 1. */
        while (list.size() < index + 1) {
            Lazy<T> tail = list.get(list.size() - 1);
            Lazy<T> next = tail.map(unaryOp);
            list.add(next);
        }

        return list.get(index).get();
    }

    public int indexOf(T value) {
        /* Simply call get() for each index till we find the value we need
        and get() will handle the creation of the elements for us. */
        int index = 0;
        while (index < maxSize) {
            if (get(index).equals(value)) {
                return index;
            }
            ++index;
        }

        // Return -1 if value is not found. I know the requirements does not need it.
        return -1;
    }
}
