class Student extends KeyableMap<Module> {
    public Student(String key) {
        super(key);    
    }

    public Student put(Module value) {
        super.put(value);
        return this;
    }
}
