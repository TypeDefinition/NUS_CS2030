import java.util.Optional;

class Roster extends KeyableMap<Student> {
	public Roster(String key) {
		super(key);
	}
	
	@Override
	public Roster put(Student value) {
		super.put(value);
		return this;
	}
	
	public String getGrade(String studentKey, String moduleKey, String assessmentKey) {
        // More verbose but easier to debug and readable.
        Optional<Student> student = this.get(studentKey);
        Optional<Module> module = student.flatMap((x) -> { return x.get(moduleKey); });
        Optional<Assessment> assessment = module.flatMap( (x) -> { return x.get(assessmentKey); });
        Optional<String> grade = assessment.map(Assessment::getGrade);
        return grade.orElse(String.format("No such record: %s %s %s", studentKey, moduleKey, assessmentKey));

        // This is hard to debug and is unreadable.
        /*
        return this.get(studentKey).
                flatMap((x) -> { return x.get(moduleKey); }).
                flatMap((x) -> { return x.get(assessmentKey); }).
                map(Assessment::getGrade).
                orElse(String.format("No such record: %s %s %s", studentKey, moduleKey, assessmentKey));
        */
	}
}
