class Module extends KeyableMap<Assessment> {
    public Module(String key) {
        super(key);
    }

    @Override
    public Module put(Assessment value) {
        super.put(value);
        return this;
    }
}
