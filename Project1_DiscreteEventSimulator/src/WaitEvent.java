package cs2030.simulator;

import java.util.List;

/**
 * An event representing a Customer waiting.
 */
public class WaitEvent extends SimulatorEvent {
    /**
     * identifier of the Server that is serving customer.
     */
    private final int serverIdentifier;
    /**
     * how long the customer has to wait before being served.
     */
    private final double waitTime;

    /**
     * Constructs a new WaitEvent with a customer, server list and server index.
     * @param customer The customer that this event represents.
     * @param serverList The list of servers that may serve this customer.
     * @param serverIdentifier The identifier of the server that the customer is waiting for.
     */
    public WaitEvent(Customer customer, List<Server> serverList, int serverIdentifier) {
        super(customer, serverList);
        this.serverIdentifier = serverIdentifier;
        this.waitTime = getServer(serverIdentifier).getNextAvailableTime() - getEventTime();
    }

    /**
     * Returns a string representation of the object.
     * Shows the event time, customer ID and the Server the customer is waiting for.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%.3f %d waits to be served by %d",
                getEventTime(),
                getCustomerID(),
                serverIdentifier);
    }

    @Override
    public double getEventTime() {
        return getCustomerArrivalTime();
    }

    /**
     * Returns how long the customer has to wait before being served.
     * @return how long the customer has to wait before being served.
     */
    public double getWaitTime() {
        return waitTime;
    }

    /**
     * Execute the event.
     * @return a ServeEvent.
     */
    @Override
    public SimulatorEvent execute() {
        replaceServer(getServer(serverIdentifier).setHasWaitingCustomer(true));
        return new ServeEvent(getCustomer(), getServerList(), serverIdentifier);
    }
}