import java.util.Scanner;
import java.util.ArrayList;
import java.util.PriorityQueue;
import cs2030.simulator.Server;
import cs2030.simulator.Customer;
import cs2030.simulator.SimulatorEvent;
import cs2030.simulator.EventComparator;
import cs2030.simulator.ArriveEvent;
import cs2030.simulator.ServeEvent;
import cs2030.simulator.LeaveEvent;
import cs2030.simulator.WaitEvent;

class Main {
    /**
     * The driver method of the program.
     * @param args These arguments are ignored.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numServers = sc.nextInt();

        // Create array of servers.
        ArrayList<Server> servers = new ArrayList<Server>();
        for (int i = 0; i < numServers; ++i) {
            servers.add(new Server(i + 1, true, false, 0));
        }

        // Create priority queue.
        PriorityQueue<SimulatorEvent> priorityQueue = 
                new PriorityQueue<SimulatorEvent>(new EventComparator());

        // Create arrive events
        int customerID = 1;
        while (sc.hasNextDouble()) {
            Customer customer = new Customer(customerID++, sc.nextDouble());
            priorityQueue.add(new ArriveEvent(customer, servers));
        }

        // Run simulation.
        double totalWaitTime = 0.0f;
        int serveCounter = 0;
        int leaveCounter = 0;

        while (!priorityQueue.isEmpty()) {
            SimulatorEvent event = priorityQueue.poll();
            System.out.println(event.toString());
            SimulatorEvent nextEvent = event.execute();

            if (nextEvent == event) {
                continue;
            }

            priorityQueue.add(nextEvent);
            if (nextEvent instanceof ServeEvent) {
                ++serveCounter;
            }
            if (nextEvent instanceof WaitEvent) {
                totalWaitTime += ((WaitEvent)nextEvent).getWaitTime();
            }
            if (nextEvent instanceof LeaveEvent) {
                ++leaveCounter;
            }
        }

        System.out.println(String.format("[%.3f %d %d]",
                totalWaitTime / serveCounter,
                serveCounter,
                leaveCounter));
    }
}
