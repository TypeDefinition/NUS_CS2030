package cs2030.simulator;

import java.util.List;

/**
 * An event representing a Customer that is done being served.
 */
public class DoneEvent extends SimulatorEvent {
    /**
     * identifier of the Server that is serving customer.
     */
    private final int serverIdentifier;
    /**
     * the time this event occurred.
     */
    private final double eventTime;

    /**
     * Constructs a new DoneEvent with a customer, server list and server index.
     * @param customer the customer that this event represents.
     * @param serverList the list of servers.
     * @param serverIdentifier The identifier of the server that the customer was served by.
     */
    public DoneEvent(Customer customer, List<Server> serverList, int serverIdentifier) {
        super(customer, serverList);
        this.serverIdentifier = serverIdentifier;
        this.eventTime = getServer(serverIdentifier).getNextAvailableTime();
    }

    /**
     * Returns a string representation of the object.
     * Shows the event time, customer ID and the Server the customer was served by.
     * @return a string representation of the object
     */
    @Override
    public String toString() {
        return String.format("%.3f %d done serving by %d",
                getEventTime(), getCustomerID(), serverIdentifier);
    }
    
    @Override
    public double getEventTime() {
        return eventTime;
    }

    /**
     * Execute the event. Since DoneEvent does not have a next stage, it will return itself.
     * @return itself
     */
    @Override
    public SimulatorEvent execute() {
        replaceServer(getServer(serverIdentifier).setIsAvailable(true));
        return this;
    }
}