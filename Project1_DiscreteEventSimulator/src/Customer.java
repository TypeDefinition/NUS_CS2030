package cs2030.simulator;

/**
 * Represents a customer.
 */
public class Customer {
    /**
      * ID of the customer.
      */
    private final int id;
    /**
      * arrival time of the customer.
      */
    private final double arrivalTime;

    /**
      * Constructs a new Customer with an ID and arrival time.
      * @param id This is the customer's ID.
                  It is assigned in chronological order of arrival.
      * @param arrivalTime The time the customer arrives.
      */
    public Customer(int id, double arrivalTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
    }

    /**
     * Returns a string representation of the Customer object, including its ID and arrival time.
     * @return a string representation of the Customer object.
     */
    @Override
    public String toString() {
        return String.format("%d arrives at %.1f", id, arrivalTime);
    }

    public int getID() {
        return id;
    }

    public double getArrivalTime() {
        return arrivalTime;
    }
}