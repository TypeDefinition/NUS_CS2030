package cs2030.simulator;

import java.util.Comparator;

/**
 * Compares 2 SimulatorEvents.
 */
public class EventComparator implements Comparator<SimulatorEvent> {
    /**
     * Compares 2 SimulatorEvents for order. Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal to,
     * or greater than the second.
     * @param eventA first event to be compared.
     * @param eventB second event to be compared.
     */ 
    public int compare(SimulatorEvent eventA, SimulatorEvent eventB) {
        if (eventA.getEventTime() < eventB.getEventTime()) {
            return -1;
        } else if (eventA.getEventTime() > eventB.getEventTime()) {
            return 1;
        }

        if (eventA.getCustomerArrivalTime() < eventB.getCustomerArrivalTime()) {
            return -1;
        } else if (eventA.getCustomerArrivalTime() > eventB.getCustomerArrivalTime()) {
            return 1;
        }

        return 0;
    }
}