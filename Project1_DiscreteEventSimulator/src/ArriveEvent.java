package cs2030.simulator;

import java.util.List;

/**
 * An event representing the arrival of a Customer.
 */
public class ArriveEvent extends SimulatorEvent {
    /**
     * Creates a new ArriveEvent with a customer and server list.
     * @param customer the customer that this event represents.
     * @param serverList the list of servers.
     */
    public ArriveEvent(Customer customer, List<Server> serverList) {
        super(customer, serverList);
    }

    /**
     * Returns a string representation of the object. Shows the event time and customer ID.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%.3f %d arrives", getEventTime(), getCustomerID());
    }

    @Override
    public double getEventTime() {
        return getCustomerArrivalTime();
    }

    /**
     * Execute the event.
     * @return a ServeEvent, WaitEvent or LeaveEvent
     *         depending on the status of the list of Servers.
     */
    @Override
    public SimulatorEvent execute() {
        List<Server> serverList = getServerList();

        // customer finds a server to serve her.
        for (Server server : serverList) {
            if (server.getIsAvailable()) {
                return new ServeEvent(getCustomer(), serverList, server.getIdentifier());
            }
        }

        // customer finds a server to wait for.
        for (Server server : serverList) {
            if (!server.getHasWaitingCustomer()) {
                return new WaitEvent(getCustomer(), serverList, server.getIdentifier());
            }
        }

        // customer leaves.
        return new LeaveEvent(getCustomer(), serverList);
    }
}