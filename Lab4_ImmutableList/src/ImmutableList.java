import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.Comparator;

class ImmutableList<T> {
    private final ArrayList<T> list;

    @SafeVarargs
    public ImmutableList(T ..._args) {
        this(Arrays.asList(_args));
    }

    public ImmutableList(List<T> _list) {
        list = new ArrayList<T>(_list);
    }

    @Override
    public String toString() {
        return list.toString();
    }

    public ImmutableList<T> add(T _element) {
        ArrayList<T> arrayList = new ArrayList<T>(list);
        arrayList.add(_element);
        return new ImmutableList<T>(arrayList);
    }

    public ImmutableList<T> remove(T _element) {
        ArrayList<T> arrayList = new ArrayList<T>(list);
        arrayList.remove(_element);
        return new ImmutableList<T>(arrayList);    
    }

    public ImmutableList<T> replace(T _oldElement, T _newElement) {
        ArrayList<T> arrayList = new ArrayList<T>(list);
        Collections.replaceAll(arrayList, _oldElement, _newElement);
        return new ImmutableList<T>(arrayList);
    }

    public ImmutableList<T> limit(long _size) {
        int size = Math.min((int)_size, list.size());

        if (_size < 0) {
            throw new IllegalArgumentException("limit size < 0"); 
        }

        ArrayList<T> arrayList = new ArrayList<T>(list.subList(0, size));
        return new ImmutableList<T>(arrayList);
    }

    @Override
    public boolean equals(Object _other) {
        return (_other instanceof ImmutableList<?>) ? list.equals(((ImmutableList<?>)_other).list) : false;
    }

    public ImmutableList<T> sorted(Comparator<? super T> _comparator) {
        if (_comparator == null) {
            throw new NullPointerException("Comparator is null");
        }

        ArrayList<T> arrayList = new ArrayList<T>(list);
        arrayList.sort(_comparator);
        return new ImmutableList<T>(arrayList);
    }

    public Object[] toArray() {
        return list.toArray();
    }

    public <U> U[] toArray(U[] _input) {
        try {
            return list.toArray(_input);
        } catch (ArrayStoreException e) {
            throw new ArrayStoreException("Cannot add element to array as it is the wrong type");
        } catch (NullPointerException e) {
            throw new NullPointerException("Input array cannot be null");
        }
    }

    public ImmutableList<T> filter(Predicate<? super T> _predicate) {
        ArrayList<T> arrayList = new ArrayList<T>();
        for (T element : list) {
            if (_predicate.test(element)) {
                arrayList.add(element);
            }
        }
        return new ImmutableList<T>(arrayList);
    }

    public <U> ImmutableList<U> map(Function<? super T, ? extends U> _func) {
        ArrayList<U> arrayList = new ArrayList<U>();
        for (T element : list) {
            arrayList.add(_func.apply(element));
        }
        return new ImmutableList<U>(arrayList);
    }
}
