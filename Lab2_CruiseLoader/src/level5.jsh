Loader[] assignLoaders(ArrayList<Loader> _loaders, Cruise _cruise) {
    Loader[] assignedLoaders = new Loader[_cruise.getNumOfLoadersRequired()];
    int assigned = 0;

    for (int i  = 0; i < _loaders.size() && assigned < _cruise.getNumOfLoadersRequired(); ++i) {
        if (_loaders.get(i).canServe(_cruise)) {
            Loader loader = _loaders.get(i).serve(_cruise);
            _loaders.set(i, loader);
            assignedLoaders[assigned++] = loader;
        }
    }

    while (assigned < _cruise.getNumOfLoadersRequired()) {
        Loader loader = new Loader(_loaders.size() + 1, _cruise);
        _loaders.add(loader);
        assignedLoaders[assigned++] = loader;
    }

    return assignedLoaders;
}

void serveCruises(Cruise[] _cruises) {
    ArrayList<Loader> loaders = new ArrayList<Loader>();

    for (Cruise cruise : _cruises) {
        Loader[] assignedLoaders = assignLoaders(loaders, cruise);
        
        for (int i = 0; i < assignedLoaders.length; ++i) {
            System.out.println(assignedLoaders[i].toString());
        }
    }
}
