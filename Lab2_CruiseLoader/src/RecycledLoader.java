class RecycledLoader extends Loader {
    public RecycledLoader(int _identifier, Cruise _cruise) {
        super(_identifier, _cruise);
    }
   
    @Override
    public String toString() {
        return "Recycled " + super.toString();
    }

    @Override
    public int getNextAvailableTime() {
        // What kinda poor materials are these things made of?
        // Seems like in the long run, the man hours and cost to maintain and run
        // these are even more destructive to the environment then just buying a
        // normal loader.
        return super.getNextAvailableTime() + 60;    
    }

    @Override
    public RecycledLoader serve(Cruise _cruise) {
        return canServe(_cruise) ? new RecycledLoader(getIdentifier(), _cruise) : this;
    }
}
