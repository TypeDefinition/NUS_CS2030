class SmallCruise extends Cruise {
    public SmallCruise(String _identifier, int _arrivalTime) {
        super(_identifier, _arrivalTime, 1, 30);
    }
}
