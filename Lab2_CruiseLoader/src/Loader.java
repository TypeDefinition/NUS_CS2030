class Loader {
       private final int identifier;
       private final Cruise cruise;

       public Loader(int _identifier, Cruise _cruise) {
           identifier = _identifier;
           cruise = _cruise;
       }

       @Override
       public String toString() {
           return "Loader " + identifier + " serving " + cruise.toString();
       }

       public int getIdentifier() {
           return identifier;
       }

       public boolean canServe(Cruise _cruise) {
           return _cruise.getArrivalTime() >= this.getNextAvailableTime();
       }

       public int getNextAvailableTime() {
           return cruise.getServiceCompletionTime();
       }

       public Loader serve(Cruise _cruise) {
           return canServe(_cruise) ? new Loader(identifier, _cruise) : this;
       }
}
