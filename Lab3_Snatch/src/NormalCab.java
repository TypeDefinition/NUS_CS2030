class NormalCab extends Driver {
	public NormalCab(String _license, int _waitTime) {
		super(_license, _waitTime);
	}

	@Override
	public String toString() {
		return super.toString() + " NormalCab";
	}

	@Override
	public Service getBestService(Request _request) {
		Service[] services = {
			new JustRide(),
			new TakeACab() 
		};

		Service best = services[0];
		for (int i = 1; i < services.length; ++i) {
			if (services[i].computeFare(_request) < best.computeFare(_request)) {
				best = services[i];
			}
		}

		return best;
	}
}
