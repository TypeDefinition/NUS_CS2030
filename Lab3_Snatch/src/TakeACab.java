class TakeACab extends Service {
	public TakeACab() {
	}

	@Override
	public String toString() {
		return "TakeACab";
	}

	@Override
	public int computeFare(Request _request) {
		return _request.getDistance() * 33 + 200;
	}
}