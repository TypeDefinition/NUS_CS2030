/open LazyList.java

LazyList<Integer> l1 = LazyList.iterate(0, i -> i < 2, i -> i + 1);
System.out.println("List 1:");
l1.forEach(x -> System.out.print(x));

LazyList<Integer> l2 = LazyList.iterate(5, i -> i < 8, i -> i + 2);
System.out.println("List 2:");
l2.forEach(x -> System.out.print(x));

LazyList<Integer> l3 = LazyList.concat(l1, l2);
l3.forEach(x -> System.out.print(x));
