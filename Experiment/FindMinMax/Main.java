import java.util.function.Function;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.Optional;
import java.util.Scanner;
import java.lang.Integer;

public class Main {
    static Optional<MinMax> findMinMax(Stream<Integer> intStream) {
        Function<Integer, MinMax> convertToMinMax = (x) -> {
            return new MinMax(x, x);
        };
        BinaryOperator<MinMax> compareMinMax = (a, b) -> {
            return new MinMax(Integer.min(a.min, b.min), Integer.max(a.max, b.max));
        };
    
        return intStream.parallel().
            map(convertToMinMax).
            reduce(compareMinMax);
    }

    public static void main(String[] args) {
        System.out.print("From range: ");
        int from = (new Scanner(System.in)).nextInt();
        System.out.print("To range: ");
        int to = (new Scanner(System.in)).nextInt();
        Stream<Integer> instream = IntStream
            .rangeClosed(from, to)
            .mapToObj(x -> Integer.valueOf(x));
        System.out.println(findMinMax(instream));
    }
}
