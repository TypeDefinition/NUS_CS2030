public class Main {
    public static void main(String[] args) {
        Undoable<Integer> i = Undoable.of("hello").map(s-> s.length());
        Undoable<Double> d = i.undo();
        System.out.println(d.get());
    }
}
