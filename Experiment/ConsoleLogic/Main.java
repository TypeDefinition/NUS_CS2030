class Main {
    public static void main(String[] args) {
        Logic logic = new Logic();
        Console[] consoles = new Console[5];
        
        for (int i = 0; i < consoles.length; ++i) {
            consoles[i] = new Console("console " + i, logic);
        }
        consoles[0].start();
    }
}
