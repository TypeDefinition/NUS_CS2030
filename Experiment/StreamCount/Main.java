import java.util.stream.IntStream;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.function.IntConsumer;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Double;

public class Main {
    public static <U> long myCount(Stream<U> stream) {
        return stream.collect(Collectors.toList()).size();
    }

    public static long countRepeats(String string) {
        if (string.length() < 2) {
            return 0;
        }
    
        if (string.length() == 2) {
            return string.charAt(0) == string.charAt(1) ? 1 : 0;
        }
    
        List<Integer> counter = new ArrayList<Integer>();
        
        IntConsumer compareAdjacent = (i) -> {
            char a = string.charAt(i);
            char b = string.charAt(i + 1);

            if (i == string.length() - 2) {
                if (a == b) { counter.add(1); }
            } else {
                char c = string.charAt(i + 2);
                if (a == b && b != c) { counter.add(1); }
            }
        };
        
        IntStream.range(0, string.length() - 1).forEach(compareAdjacent);
        
        return counter.size();
    }

    public static Optional<Double> variance(int[] intArray) {
        if (intArray.length < 2) {
            return Optional.empty();
        }
        
        DoubleStream a = Arrays.stream(intArray).
            mapToDouble((x) -> Double.valueOf((double)x));
        double average = a.sum() / (double)intArray.length;
        
        DoubleStream b = Arrays.stream(intArray).
            mapToDouble((x) -> Double.valueOf((double)x)).
            map((x) -> (x - average) * (x - average));
        double result = b.sum() / (double)(intArray.length - 1);
        
        return Optional.of(result);
    }
    
    public static String reverse(String string) {
        char[] reverseString = new char[string.length()];

        IntStream.range(0, string.length()).
            boxed().parallel().
            forEach((i) -> { reverseString[i] = string.charAt(string.length() - 1 - i); });
            
        return new String(reverseString);
    }

    public static void main(String[] args) {
        Stream<Integer> stream = IntStream.range(0, 1000).boxed();
        System.out.println(String.format("Size of stream is %d.", myCount(stream)));
        
        System.out.println(String.format("Number of repeats is %d.", countRepeats("mississssssippihhhhhh")));
        
        System.out.println(String.format("Variance is %.3f", variance(IntStream.rangeClosed(1, 6).toArray()).get()));
        
        System.out.println(String.format("Reverse of Fuck is %s", reverse("Fuck")));
    }
}
