import java.util.List;
import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

public class Main {
    static void HighSearch(int[] intArray, int numLoop) {
        long totalDuration = 0;
        for (int i = 0; i < numLoop; ++i) {
            long startTime = System.nanoTime();
            boolean result = new BinSearch(0, intArray.length, intArray.length - 1, intArray).compute();
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);            
            totalDuration += duration;
            System.out.println(String.format("High Search Duration: %b %dns", result, duration));
        }
        long averageDuration = totalDuration / numLoop;
        System.out.println(String.format("High Search Total Duration: %dns", totalDuration));
        System.out.println(String.format("High Search Average Duration: %dns", averageDuration));
    }
    
    static void LowSearch(int[] intArray, int numLoop) {
        long totalDuration = 0;
        for (int i = 0; i < numLoop; ++i) {
            long startTime = System.nanoTime();
            boolean result = new BinSearch(0, intArray.length, 0, intArray).compute();
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);            
            totalDuration += duration;
            System.out.println(String.format("Low Search Duration: %b %dns", result, duration));
        }
        long averageDuration = totalDuration / numLoop;
        System.out.println(String.format("Low Search Total Duration: %dns", totalDuration));
        System.out.println(String.format("Low Search Average Duration: %dns", averageDuration));
    }

    public static void main(String[] args) {
        int[] intArray = new int[999999999];
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = i;
        }
        
        int numLoop = 10;
        
        System.out.println("For caching purposes. Ignore results.");
        HighSearch(intArray, 5);
        System.out.println("");
        
        System.out.println("Actual test starts.");
        HighSearch(intArray, numLoop);
        System.out.println("");
        
        System.out.println("For caching purposes. Ignore results.");
        LowSearch(intArray, 5);
        System.out.println("");
        
        System.out.println("Actual test starts.");
        LowSearch(intArray, numLoop);
        System.out.println("");
    }
}
