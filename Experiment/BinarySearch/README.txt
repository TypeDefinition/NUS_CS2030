CS2030 AY17/18 Final Assessment Question 10

This experiment compares the overhead of using fork() and join() as compared to doing the same task sequentially on the main thread.
