public class Main {
    static Compute<Long> sum(long n, long s) {
        if (n == 0) {
            return new Base<>(() -> s);
        } else {
            return new Recursive<>(() -> sum(n - 1, n + s));
        }
    }
    
    public static void main(String[] args) {
        System.out.println("Sum: " + sum(100000, 0).getValue());
    }
}
