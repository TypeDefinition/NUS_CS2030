import java.util.function.Supplier;

public class Base<T> extends Compute<T> {
    private final Supplier<T> supplier;

    public Base(Supplier<T> supplier) {
        this.supplier = supplier;
    }
    
    @Override
    T getValue() {
        return supplier.get();
    }
}
