public abstract class Compute<T> {
    public Compute() {}
    
    Compute<T> getNext() {
        return this;
    }
    
    abstract T getValue();
}
