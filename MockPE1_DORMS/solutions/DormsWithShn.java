import java.util.List;
/**
 * DormsWithShn is the same as DORMS, but with SHN implemented.
 */
public class DormsWithShn extends Dorms {

    /**
     * Initialises DORMS with SHN policy
     * @param verbose Whether the program is run in verbose mode
     */
    DormsWithShn(boolean verbose) {
        super(verbose);
    }

    @Override
    void handleSickPerson(Person person, double time) {
        // Log whatever is necessary
        super.handleSickPerson(person, time);

        // Check if person has been tested positive
        if (!person.test(SimulationParameters.TARGET_VIRUS)) {
            return;
        }

        // Serve a SHN to the sick person and log.
        serveSHN(person.toString(), time + (SimulationParameters.SHN_DURATION * 2));

        // Get each contacted person and serve him/her the SHN and log it.
        for (Contact c : this.queryContacts(person.toString(), time)) {
            
            double shnEndTime = c.timeOfContact() +
                    SimulationParameters.SHN_DURATION;
            
            List<Person> contacts = c.getPeople();
            for (Person p : contacts) {
                serveSHN(p.toString(), shnEndTime);
            }
        }
    }

    private void serveSHN(String personName, double shnEndTime) {
        Person person = getUpdatedPerson(personName);
        Person result = person.serveSHN(shnEndTime);
        if (!person.equals(result)) {
            updatePerson(result);
            logServeSHN(result, shnEndTime);
        }
    }

    private void logServeSHN(Person person, double shnEndTime) {
        log(String.format("%s has been served a SHN that ends at %.3f", 
                    person, shnEndTime));
    }

}

