import java.util.ArrayList;
import java.util.List;

/**
 * A MaskedPerson is a {@link Person} wearing a mask
 */
class MaskedPerson extends Person {

    /**
     * Creates a MaskedPerson
     * @param name The name of the person
     */
    MaskedPerson(String name) {
        super(name);
    }

    /**
     * Clones a {@link Person} into a MaskedPerson
     * @param p The person to clone
     */
    MaskedPerson(Person p) {
        super(p);
    }

    @Override
    List<Virus> transmit(double random) {
        List<Virus> toSpread = new ArrayList<>();
        if (random <= SimulationParameters.MASK_EFFECTIVENESS) {
            return toSpread;
        }
        return super.transmit(random);
    }

    /**
     * Infects this person with a list of viruses
     * @param listOfViruses The list of viruses to infect this person with
     * @param random        the random value to generate the outcome of
     *                      infection
     * @return              The resulting Person.
     */
    Person infectWith(List<Virus> listOfViruses, double random) {
        if (random <= SimulationParameters.MASK_EFFECTIVENESS) {
            return this;
        }
        return new MaskedPerson(super.infectWith(listOfViruses, random));
    }

    /**
     * Serves this person with a Stay Home Notice
     * @param shnEndTime the end time of the stay home notice
     * @return the resulting Person on SHN 
     */
    MaskedPerson serveSHN(double shnEndTime) {
        if (shnEndTime <= this.shnEndTime) {
            return this;
        }
        return new MaskedPerson(super.serveSHN(shnEndTime));
    }
}

