import java.util.LinkedList;
import java.util.List;

/**
 * The Location class represents a location
 */
class Location {

    private final String name;
    private final List<Person> listOfPersonsInLocation;

    /**
     * Creates a location
     * @param name the name of the location
     */
    Location(String name) {
        this(name, new LinkedList<>());
    }

    /**
     * Creates a location
     * @param name the name of the location
     * @param listOfPersonsInLocation The list of people in the location
     */
    Location(String name, List<Person> listOfPersonsInLocation) {
        this.name = name;
        this.listOfPersonsInLocation = listOfPersonsInLocation;
    }

    /**
     * Gets all the occupants in the location
     * @return all the occupants in the location
     */
    List<Person> getOccupants() {
        return List.copyOf(this.listOfPersonsInLocation);
    }

    /**
     * Accepts a person into this location
     * @param person The person to accept
     */
    Location accept(Person person) {
        List<Person> newList = new LinkedList<>();
        newList.addAll(this.listOfPersonsInLocation);
        newList.add(person);
        return new Location(this.name, newList);
    }

    /**
     * Removes a person from this location
     * @param personName The name of the person to remove
     */
    Location remove(String personName) {
        List<Person> newList = new LinkedList<>();
        newList.addAll(this.listOfPersonsInLocation);
        for (int i = 0; i < newList.size(); ++i) {
            if (newList.get(i).toString().equals(personName)) {
                newList.remove(i);
            }
        }
        return new Location(this.name, newList);
        
    }

    @Override
    public String toString() {
        return this.name;
    }

}

