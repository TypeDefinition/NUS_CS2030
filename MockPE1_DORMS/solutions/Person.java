import java.util.ArrayList;
import java.util.List;

/**
 * Represents the regular human
 */
class Person {

    private final String name;
    private final List<Virus> listOfViruses;
    final double shnEndTime;

    /**
     * Creates a person
     * @param name The name of the person
     */
    Person(String name) {
        this(name, List.of(), -1);
    }

    /**
     * Creates a person given the name and the list of viruses
     * he/she is infected with
     * @param name          The name of the person
     * @param listOfViruses The list of {@link Virus viruses} the person
     *                      is infected with
     */
    Person(String name, List<? extends Virus> listOfViruses) {
        this(name, List.copyOf(listOfViruses), -1);
    }

    /**
     * Creates a new Person given all three parameters
     * @param name          The name of the Person
     * @param listOfViruses The list of {@link Virus viruses} the person
     *                      is infected with
     * @param shnEndTime    The end time of the SHN
     */
    Person(String name, List<? extends Virus> listOfViruses, double shnEndTime) {
        this.name = name;
        this.listOfViruses = List.copyOf(listOfViruses);
        this.shnEndTime = shnEndTime;
    }

    /**
     * Essentially creates a clone of a Person
     * @param p The person to clone
     */
    Person(Person p) {
        this(p.name, p.listOfViruses, p.shnEndTime);
    }

    /**
     * Transmits some viruses out of this person
     * @param random The random value to generate the outcome
     *               of disease transmission
     * @return       the viruses to transmit
     */
    List<Virus> transmit(double random) {
        List<Virus> toSpread = new ArrayList<>();
        for (Virus v : this.listOfViruses) {
            toSpread.add(v.spread(random));
        }
        return toSpread;
    }

    /**
     * Infects this person with a list of viruses
     * @param listOfViruses The list of viruses to infect this person with
     * @param random        the random value to generate the outcome of
     *                      infection
     * @return              The resulting Person.
     */
    Person infectWith(List<Virus> listOfViruses, double random) {
        List<Virus> allViruses = new ArrayList<>();
        allViruses.addAll(this.listOfViruses);
        allViruses.addAll(listOfViruses);
        return new Person(this.name, allViruses);
    }

    /**
     * Tests this human for a virus
     * @param virusName The name of the virus
     * @return true if this person is infected with the target virus
     */
    boolean test(String virusName) {
        for (Virus v : listOfViruses) {
            if (v.test(virusName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Serves this person with a Stay Home Notice
     * @param shnEndTime the end time of the stay home notice
     * @return the resulting Person on SHN 
     */
    Person serveSHN(double shnEndTime) {
        if (shnEndTime <= this.shnEndTime) {
            return this;
        }
        return new Person(this.name, this.listOfViruses, shnEndTime);
    }

    /**
     * Checks if this person is on Stay Home Notice
     * @param currentTime the time to check
     * @return true if at {@code currentTime}, the person is still
     *          on Stay Home Notice
     */
    boolean onSHN(double currentTime) {
        return currentTime < this.shnEndTime;
    }

}

