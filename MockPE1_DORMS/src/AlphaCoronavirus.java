class AlphaCoronavirus extends Virus {
    public AlphaCoronavirus(double probabilityOfMutating) {
        super("Alpha Coronavirus", probabilityOfMutating);
    }

    @Override
    public Virus spread(double random) {
        if (random > getProbabilityOfMutating()) {
            return new AlphaCoronavirus(getProbabilityOfMutating() * SimulationParameters.VIRUS_MUTATION_PROBABILITY_REDUCTION);
        }
        return new SARS_CoV_2(getProbabilityOfMutating()); 
    }
}
