import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

class DormsWithShn extends Dorms {
    public DormsWithShn(boolean verbose) {
        super(verbose);
    }

    @Override
    void handleSickPerson(Person person, double time) {
        super.handleSickPerson(person, time);

        if (!person.test(SimulationParameters.TARGET_VIRUS)) {
            return;
        }

        Person sickPerson = getUpdatedPerson(person.getName());
        sickPerson = sickPerson.serveSHN(time + SimulationParameters.SHN_DURATION * 2.0);
        updatePerson(sickPerson);
        log(String.format("%s has been served a SHN that ends at %.3f", sickPerson.getName(), sickPerson.getSHNEndTime()));

        List<Contact> contactList = new ArrayList<Contact>(queryContacts(person.getName(), time));
        for (Contact contact : contactList) {
            List<Person> contactPeople = contact.getPeople();
            for (Person contactPerson : contactPeople) {
                if (contactPerson.nameIs(sickPerson.getName())) {
                    continue;
                }

                Person contactPersonUpdated = getUpdatedPerson(contactPerson.getName());
                contactPersonUpdated = contactPersonUpdated.serveSHN(contact.timeOfContact() + SimulationParameters.SHN_DURATION);
                updatePerson(contactPersonUpdated);
                log(String.format("%s has been served a SHN that ends at %.3f", contactPersonUpdated.getName(), contactPersonUpdated.getSHNEndTime()));
            }
        }
    }
}
