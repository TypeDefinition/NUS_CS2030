import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

class Contact {
    private Person first;
    private Person second;
    private final double time;

    public Contact(Person first, Person second, double time) {
        this.first = first;
        this.second = second;
        this.time = time;
    }

    public List<Person> getPeople() {
        return new ArrayList<Person>(Arrays.asList(first, second));
    }

    public double timeOfContact() {
        return time;
    }

    public List<Person> transmit(double random) {
        List<Virus> firstVirus = first.transmit(random);
        List<Virus> secondVirus = second.transmit(random);
        Person firstInfected = first.infectWith(secondVirus, random);
        Person secondInfected = second.infectWith(firstVirus, random);
        return new ArrayList<Person>(Arrays.asList(firstInfected, secondInfected));
    }
}
