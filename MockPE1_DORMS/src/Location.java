import java.util.List;
import java.util.ArrayList;

class Location {
    private final String name;
    private final List<Person> occupants;

    public Location(String name, List<Person> occupants) {
        this.name = name;
        this.occupants = occupants;
    }

    public Location(String name) {
        this(name, new ArrayList<Person>());
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public List<Person> getOccupants() {
        return occupants;
    }

    public boolean contains(String personName) {
        for (Person person : occupants) {
            if (person.nameIs(personName)) {
                return true;
            }
        }
        return false;
    }

    public Location accept(Person person) {
        ArrayList<Person> newOccupants = new ArrayList<Person>();
        for (Person p : occupants) {
            if (!p.nameIs(person.getName())) {
                newOccupants.add(p);
            }
        }
        newOccupants.add(person);
        return new Location(name, newOccupants);
    }

    public Location remove(String personName) {
        if (!contains(personName)) {
            throw new IllegalArgumentException(String.format("This location does not contain anyone with the name %s.", personName));
        }

        ArrayList<Person> newOccupants = new ArrayList<Person>(occupants);
        newOccupants.removeIf((Person occupant) -> { return occupant.nameIs(personName); });
        return new Location(name, newOccupants);
    }
}
